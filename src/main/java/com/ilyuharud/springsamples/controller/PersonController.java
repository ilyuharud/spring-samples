package com.ilyuharud.springsamples.controller;

import com.ilyuharud.springsamples.domain.dto.ReadPersonRequest;
import com.ilyuharud.springsamples.domain.dto.ReadPersonResponse;
import com.ilyuharud.springsamples.domain.entity.Person;
import com.ilyuharud.springsamples.domain.exception.PersonNotFoundException;
import com.ilyuharud.springsamples.service.PersonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@Slf4j
@Validated
@RestController
@RequestMapping
public class PersonController {
    @Autowired
    private PersonService personService;

    @GetMapping("/people")
    public ResponseEntity<List<ReadPersonResponse>> readPersons(@RequestBody ReadPersonRequest readPersonRequest) {
        log.info("Request param: " + readPersonRequest.toString());
        return ResponseEntity.ok(personService.readPeople(readPersonRequest));
    }

    @GetMapping("/people/{id}")
    public ResponseEntity<Person> readPerson(@PathVariable(value = "id") long id) throws PersonNotFoundException {
            return ResponseEntity.ok(personService.readPerson(id));
    }

    @PostMapping("/people")
    public ResponseEntity<Person> createPerson(@Valid @RequestBody Person person) throws URISyntaxException {
        Person createdPerson = personService.createPerson(person);
        URI location = new URI("/people/" + createdPerson.getId());
        return ResponseEntity
                .created(location)
                .body(createdPerson);
    }
}

package com.ilyuharud.springsamples.controller.advice;

import com.ilyuharud.springsamples.domain.dto.Violation;
import com.ilyuharud.springsamples.domain.exception.PersonNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ErrorHandler {

    @ResponseBody
    @ExceptionHandler(PersonNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Violation employeeNotFondHandler(PersonNotFoundException ex) {
        return Violation.builder()
                .type(HttpStatus.NOT_FOUND.value())
                .message(ex.getMessage())
                .build();
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Violation handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Violation violation = new Violation();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String errorMessage = error.getDefaultMessage();
            violation.setType(HttpStatus.BAD_REQUEST.value());
            violation.setMessage(errorMessage);
        });
        return violation;
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public Violation handleConflict(HttpMessageNotReadableException ex) {
        return Violation.builder()
                .type(HttpStatus.BAD_REQUEST.value())
                .message("Пошел на хуй")
                .build();
    }
}

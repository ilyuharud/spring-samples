package com.ilyuharud.springsamples.validation;

import com.ilyuharud.springsamples.validation.validator.HobbyCountryNotEmptyValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = {HobbyCountryNotEmptyValidator.class})
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface HobbyCountryNotEmpty {
    String message() default "Person not valid";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}

package com.ilyuharud.springsamples.validation.validator;

import com.ilyuharud.springsamples.domain.entity.Person;
import com.ilyuharud.springsamples.validation.HobbyCountryNotEmpty;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class HobbyCountryNotEmptyValidator implements ConstraintValidator<HobbyCountryNotEmpty, Person> {
    @Override
    public void initialize(HobbyCountryNotEmpty constraintAnnotation) {

    }

    @Override
    public boolean isValid(Person person, ConstraintValidatorContext constraintValidatorContext) {
        if (person.getHobby() == null && person.getCountry() == null)
            return false;
        return  (!person.getHobby().isEmpty() || !person.getCountry().isEmpty());
    }
}

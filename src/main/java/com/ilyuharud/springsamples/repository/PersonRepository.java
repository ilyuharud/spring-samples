package com.ilyuharud.springsamples.repository;

import com.ilyuharud.springsamples.domain.dto.ReadPersonResponse;
import com.ilyuharud.springsamples.domain.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface PersonRepository extends JpaRepository<Person, Long> {

    @Query(value = "SELECT * FROM dim_person WHERE country = :country", nativeQuery = true)
    List<Person> findAllByCountry(@Param("country") String country);

    @Query(value = "SELECT * FROM dim_person WHERE id = :id", nativeQuery = true)
    Optional<Person> findById(@Param("id") Long id);
}

package com.ilyuharud.springsamples.service;

import com.ilyuharud.springsamples.domain.dto.ReadPersonRequest;
import com.ilyuharud.springsamples.domain.dto.ReadPersonResponse;
import com.ilyuharud.springsamples.domain.entity.Person;
import com.ilyuharud.springsamples.domain.exception.PersonNotFoundException;
import com.ilyuharud.springsamples.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class PersonService {
    @Autowired
    private PersonRepository personRepository;

    public List<ReadPersonResponse> readPeople(ReadPersonRequest request) {
        return personRepository.findAllByCountry(request.getCountry())
                .stream()
                .collect(Collectors.groupingBy(Person::getName, Collectors.mapping(Person::getHobby, Collectors.toList())))
                .entrySet()
                .stream()
                .map(this::requestFromEntry)
                .collect(Collectors.toList());
    }

    public Person readPerson(Long id) throws PersonNotFoundException {
        return personRepository.findById(id)
                .orElseThrow(PersonNotFoundException::new);
    }

    public Person createPerson(Person person) {
        return personRepository.save(person);
    }

    private ReadPersonResponse requestFromEntry(Map.Entry<String, List<String>> e) {
        return new ReadPersonResponse(e.getKey(), e.getValue());
    }
}

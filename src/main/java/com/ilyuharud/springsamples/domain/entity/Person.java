package com.ilyuharud.springsamples.domain.entity;


import com.ilyuharud.springsamples.validation.HobbyCountryNotEmpty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "dim_person")
@Validated
@AllArgsConstructor
@NoArgsConstructor
@HobbyCountryNotEmpty
public class Person {
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    @NotBlank(message = "Name is mandatory")
    String name;
    @NotNull(message = "Country can not be null")
    String country;
    @NotNull(message = "Hobby can not be null")
    String hobby;

    public Person(String name, String country, String hobby) {
        this.name = name;
        this.country = country;
        this.hobby = hobby;
    }
}

package com.ilyuharud.springsamples.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ReadPersonResponse {
    private String name;
    private List<String> hobbies;
}

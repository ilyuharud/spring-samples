package com.ilyuharud.springsamples;

import com.ilyuharud.springsamples.domain.entity.Person;
import com.ilyuharud.springsamples.repository.PersonRepository;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.PostgreSQLContainer;

import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource({"classpath:application-test.properties"})
public class PersonRepositoryTest {

    @Autowired
    private PersonRepository personRepository;

    @ClassRule
    public static PostgreSQLContainer postgreSQLContainer = BaeldungPostgresqlContainer.getInstance();

    @Test
    public void testPersonRepo() {
        insertPeople();
        List<Person> response = personRepository.findAllByCountry("Russia");
        assert response.size() == 2;
    }

    private void insertPeople() {
        List<Person> people = Arrays.asList(
                new Person("Ilya", "Russia","dev"),
                new Person("Ilya", "Russia","cats"),
                new Person("Max", "Ukraine","dev"),
                new Person("Max", "Ukraine","dogs")
        );
        personRepository.saveAll(people);
    }
}

package com.ilyuharud.springsamples;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.ilyuharud.springsamples.controller.PersonController;
import com.ilyuharud.springsamples.domain.entity.Person;
import com.ilyuharud.springsamples.domain.exception.PersonNotFoundException;
import com.ilyuharud.springsamples.service.PersonService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.CoreMatchers.is;

@RunWith(SpringRunner.class)
@WebMvcTest(PersonController.class)
@TestPropertySource({"classpath:application-test.properties"})
public class PersonControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private PersonController personController;

    @MockBean
    private PersonService personService;

    private final ObjectMapper mapper = new ObjectMapper();

    @Before
    public void setUp() throws PersonNotFoundException {
        when(personService.readPerson(1L)).thenReturn(new Person("Ilya", "Russia", "Dogs"));
        when(personService.readPerson(2L)).thenThrow(new PersonNotFoundException());
    }

    @Test
    public void getPersonById_returnStatus200() throws Exception {
        this.mvc.perform(get("/people/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("Ilya")));
    }

    @Test
    public void getPersonById_returnStatus201() throws Exception {
        when(personService.createPerson(any())).thenReturn(new Person(1L, "Ilya", "", "Dogs"));
        this.mvc.perform(
                post("/people")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(new Person("Ilya", "", ""))))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getPersonById_returnStatusNotFound() throws Exception {
        this.mvc.perform(get("/people/2"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

}
